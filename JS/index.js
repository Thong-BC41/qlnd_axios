
const BASE_URL= "https://63bea7fa585bedcb36b31a59.mockapi.io"
var DSND=[]
function fetchQlndList(){
  batLoading()
    axios({
        url: `${BASE_URL}/qlnd`,
        method : "GET",
    })
      .then(function(res){
        tatLoading()
        renderQlndList(res.data)
        DSND.push(...res.data)
      })
      .catch(function(err){
        tatLoading()
      })
     
}

fetchQlndList()

 //xóa nd
 function xoaNguoiDung(id){
  batLoading()
  axios({
      url : `${BASE_URL}/qlnd/${id}`,
      method : "DELETE"
  }).then(function(res){
    tatLoading()
    fetchQlndList()
    })
    .catch(function(err){
      tatLoading()
    })
}

//thêm nd

function themNguoiDung(){
  batLoading()
  var nd=layThongTinTuForm()
  var isValid=
  kiemTraTrung(nd.taiKhoan,DSND)&&
  kiemTraTk(nd.taiKhoan)
  
  isValid= isValid & 
  kiemTraHt(nd.hoTen)&
  kiemTraMatKhau(nd.matKhau)&
  kiemTraEmail(nd.email)&
  kiemTraHinhAnh(nd.hinhAnh)&
  kiemTraNguoiDung(nd.loaiND)&
  kiemTraNgonNgu(nd.ngonNgu)&
  kiemTraDoDai(nd.moTa,"tbmota",60)
  
  if(isValid==true){
  axios({
    url : `${BASE_URL}/qlnd`,
    method : "POST",
    data : layThongTinTuForm()
  }).then(function(res){
    tatLoading()
    fetchQlndList()
    })
    .catch(function(err){
      tatLoading()
    })
 }
}

//sửa thồn tin người dùng

function suaNguoiDung(id){
  batLoading()
  axios({
    url : `${BASE_URL}/qlnd/${id}`,
    method: "GET"
  }).then(function(res){
    showThongTinRaForm(res.data)
    tatLoading()
    })
    .catch(function(err){
      tatLoading()
    })
    document.getElementById('TaiKhoan').disabled=true
    document.getElementById('capNhat').style.display="inline-block"
    document.getElementById ('themND').style.display="none"
}

function capNhatNguoiDung(){
  batLoading()
  var nd=layThongTinTuForm()
  var isValid=kiemTraHt(nd.hoTen)
  isValid= isValid &
  kiemTraHt(nd.hoTen)&
  kiemTraMatKhau(nd.matKhau)&
  kiemTraEmail(nd.email)&
  kiemTraHinhAnh(nd.hinhAnh)&
  kiemTraNguoiDung(nd.loaiND)&
  kiemTraNgonNgu(nd.ngonNgu)&
  kiemTraDoDai(nd.moTa,"tbmota",60)

  if(isValid==true){
  var data=layThongTinTuForm()
  axios({
    url: `${BASE_URL}/qlnd/${data.idStt}`,
    method : "PUT" ,
    data : data,
  }).then(function(res){
    tatLoading()
    fetchQlndList()
    })
    .catch(function(err){
      tatLoading()
    })
  }
}

function timKiem(){
  var search=document.getElementById('search').value
  var ndSearch=DSND.filter(function(value){
    return value.loaiND.toUpperCase().includes(search.toUpperCase())
  })
  renderQlndList(ndSearch)
}

function cloSe(){
  location.reload()
}