
function renderQlndList(qlndArr){
    var contentHTML=""
    qlndArr.forEach(function(qlnd) {
       var contentTr=`
       <tr>
        <td>${qlnd.idStt}</td>
        <td>${qlnd.taiKhoan}</td>
        <td>${qlnd.matKhau}</td>
        <td>${qlnd.hoTen}</td>
        <td>${qlnd.email}</td>
        <td>${qlnd.ngonNgu}</td>
        <td>${qlnd.loaiND}</td>
        <td>${qlnd.moTa}</td>
        <td>${qlnd.hinhAnh}</td>
        <td> <button onclick="xoaNguoiDung(${qlnd.idStt})" 
        class="btn btn-danger">Xóa</button></td>
        <td> <button onclick="suaNguoiDung(${qlnd.idStt})" 
        class="btn btn-danger" data-toggle="modal"
        data-target="#myModal">Sửa</button></td>
       </tr>
       `
     contentHTML += contentTr
   
    });
    document.getElementById('tblDanhSachNguoiDung').innerHTML=contentHTML
   }

  function layThongTinTuForm(){
    var idStt=document.getElementById('sTTId').value
    var taiKhoan=document.getElementById('TaiKhoan').value
    var matKhau=document.getElementById('MatKhau').value
    var hoTen=document.getElementById('HoTen').value
    var email=document.getElementById('Email').value
    var ngonNgu=document.getElementById('loaiNgonNgu').value
    var loaiND=document.getElementById('loaiNguoiDung').value
    var moTa=document.getElementById('MoTa').value
    var hinhAnh=document.getElementById('HinhAnh').value
    return {
        idStt :idStt,
        taiKhoan : taiKhoan,
        matKhau : matKhau,
        hoTen :hoTen ,
        email : email ,
        ngonNgu : ngonNgu ,
        loaiND : loaiND ,
        moTa : moTa,
        hinhAnh : hinhAnh,
    }
  }

function showThongTinRaForm(qlnd){
    document.getElementById('sTTId').value=qlnd.idStt
    document.getElementById('TaiKhoan').value=qlnd.taiKhoan
    document.getElementById('MatKhau').value=qlnd.matKhau
    document.getElementById('HoTen').value=qlnd.hoTen
    document.getElementById('Email').value=qlnd.email
    document.getElementById('loaiNgonNgu').value=qlnd.ngonNgu
    document.getElementById('loaiNguoiDung').value=qlnd.loaiND
    document.getElementById('MoTa').value=qlnd.moTa
    document.getElementById('HinhAnh').value=qlnd.hinhAnh
}

function batLoading(){
  document.getElementById('loading').style.display="flex"
}

function tatLoading(){
  document.getElementById('loading').style.display="none"
}