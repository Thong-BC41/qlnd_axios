
function kiemTraTrung(idND,ndArr){
    var viTri=ndArr.findIndex(function(item){
        return item.taiKhoan==idND
    })
    if (viTri !=-1){
        document.getElementById('tbtaikhoan').style.display="inline-block"
        document.getElementById('tbtaikhoan').innerText="Mã người dùng đã tồn tại"
        return false
    }else{
        document.getElementById('tbtaikhoan').innerText=""
        return true 
    } 
}

function kiemTraTk(value){
    if(value==""){
        document.getElementById('tbtaikhoan').style.display="inline-block"
        document.getElementById('tbtaikhoan').innerHTML="Không được để trống"
        return false
    }else{
        document.getElementById('tbtaikhoan').innerHTML=""
        return true
    }
}

function kiemTraHt(value){
    var re=/^[a-zA-ZàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬđĐèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆìÌỉỈĩĨíÍịỊòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰỳỲỷỶỹỸýÝỵỴ\s]+$/     
    var isHT=re.test(value)
    if( value==null || value==""){
        document.getElementById('tbhoten').style.display="inline-block"
        document.getElementById("tbhoten").innerText="Không được để trống" 
        return false
    }else if(isHT){
        document.getElementById("tbhoten").innerText=""
        return true
    }else{
        document.getElementById('tbhoten').style.display="inline-block"
        document.getElementById("tbhoten").innerText="Không được chứa số và ký tự đặc biệt"
        return false
    }
}

function kiemTraMatKhau(value){
    var re=/^(?=.*[A-Z])(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,8}$/
    var isPass=re.test(value)
    if(value==null || value==""){
        document.getElementById('tbmatkhau').style.display="inline-block"
        document.getElementById('tbmatkhau').innerHTML="Không được để trống"
        return false
    }else if(isPass){
        document.getElementById('tbmatkhau').innerHTML=""
        return true
    }else {
        document.getElementById('tbmatkhau').style.display="inline-block"
        document.getElementById('tbmatkhau').innerHTML=`mật Khẩu từ 6-8 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)`
        return false
    }
}

function kiemTraEmail(value){
    var re=/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    isEmail=re.test(value)
    if(value==null || value==""){
        document.getElementById('tbemail').style.display="inline-block"
        document.getElementById('tbemail').innerText="Không được để trống"
        return false
    }else if(isEmail){
        document.getElementById('tbemail').innerText=""
        return true
    }else{
        document.getElementById('tbemail').style.display="inline-block"
        document.getElementById('tbemail').innerText="Email không hợp lệ" 
        return false
    }
}

function kiemTraHinhAnh(value){
    if(value==null || value==""){
        document.getElementById('tbhinhanh').style.display="inline-block"
        document.getElementById('tbhinhanh').innerText="Không được để trống"
        return false
    }else{
        document.getElementById('tbhinhanh').innerText=""
        return true
    }
}

function kiemTraNguoiDung(value){
    if(value==null || value==""){
        document.getElementById('tbloaind').style.display="inline-block"
        document.getElementById('tbloaind').innerHTML="Không được để trống"
        return false
    }else
    if(value=="Chọn loại người dùng"){
        document.getElementById('tbloaind').style.display="inline-block"
        document.getElementById('tbloaind').innerHTML=`Chưa chọn loại người dùng`
        return false
    }else{
        document.getElementById('tbloaind').innerHTML=""
        return true
    }
}
   
function kiemTraNgonNgu(value){
    if(value==null || value==""){
        document.getElementById('tbloainn').style.display="inline-block"
        document.getElementById('tbloainn').innerHTML="Không được để trống"
        return false
    }else
    if(value=="Chọn ngôn ngữ"){
        document.getElementById('tbloainn').style.display="inline-block"
        document.getElementById('tbloainn').innerHTML=`Chưa chọn loại ngôn ngữ`
        return false
    }else{
        document.getElementById('tbloainn').innerHTML=""
        return true
    }
}

function kiemTraDoDai(value,idErr,max){
    var length=value.length
    if(value==null || value==""){
        document.getElementById(idErr).style.display="inline-block"
        document.getElementById(idErr).innerText="Không được để trống"
        return false
    }else
    if(length>max){
        document.getElementById(idErr).style.display="inline-block"
        document.getElementById(idErr).innerHTML=`Độ dài quá ${max} ký tự`
        return false
    }else{
        document.getElementById(idErr).innerHTML=""
        return true
    }
}